
use std::io::BufReader;
use std::io::Cursor;
use rodio::{Source, Sink, Decoder};

fn main() {
    let device = rodio::default_output_device()
        .expect("unable to open output device");
    let file = include_bytes!("../media/a-cruel-angels-thesis.flac").to_vec();
    let source = Decoder::new(BufReader::new(Cursor::new(file)))
        .expect("unable to create cursor (fake file)")
        .repeat_infinite();
    let sink = Sink::new(&device);
    sink.append(source);
    sink.sleep_until_end();
}


